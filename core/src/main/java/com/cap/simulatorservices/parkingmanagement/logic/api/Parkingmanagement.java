package com.cap.simulatorservices.parkingmanagement.logic.api;

import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingLotCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingLotEto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingLotSearchCriteriaTo;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotEto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotSearchCriteriaTo;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotStatusCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotStatusEto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotStatusSearchCriteriaTo;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ReservationCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ReservationEto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ReservationSearchCriteriaTo;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Interface for Parkingmanagement component.
 */
public interface Parkingmanagement {

  /**
   * Returns a ParkingLot by its id 'id'.
   *
   * @param id The id 'id' of the ParkingLot.
   * @return The {@link ParkingLotEto} with id 'id'
   */
  ParkingLotEto findParkingLot(Long id);

  /**
   * Returns a paginated list of ParkingLots matching the search criteria.
   *
   * @param criteria the {@link ParkingLotSearchCriteriaTo}.
   * @return the {@link List} of matching {@link ParkingLotEto}s.
   */
  PaginatedListTo<ParkingLotEto> findParkingLotEtos(ParkingLotSearchCriteriaTo criteria);

  /**
   * Deletes a parkingLot from the database by its id 'parkingLotId'.
   *
   * @param parkingLotId Id of the parkingLot to delete
   * @return boolean <code>true</code> if the parkingLot can be deleted, <code>false</code> otherwise
   */
  boolean deleteParkingLot(Long parkingLotId);

  /**
   * Saves a parkingLot and store it in the database.
   *
   * @param parkingLot the {@link ParkingLotEto} to create.
   * @return the new {@link ParkingLotEto} that has been saved with ID and version.
   */
  ParkingLotEto saveParkingLot(ParkingLotEto parkingLot);

  /**
   * Returns a composite ParkingLot by its id 'id'.
   *
   * @param id The id 'id' of the ParkingLot.
   * @return The {@link ParkingLotCto} with id 'id'
   */
  ParkingLotCto findParkingLotCto(Long id);

  /**
   * Returns a paginated list of composite ParkingLots matching the search criteria.
   *
   * @param criteria the {@link ParkingLotSearchCriteriaTo}.
   * @return the {@link List} of matching {@link ParkingLotCto}s.
   */
  PaginatedListTo<ParkingLotCto> findParkingLotCtos(ParkingLotSearchCriteriaTo criteria);

  /**
   * Returns a ParkingSlot by its id 'id'.
   *
   * @param id The id 'id' of the ParkingSlot.
   * @return The {@link ParkingSlotEto} with id 'id'
   */
  ParkingSlotEto findParkingSlot(Long id);

  /**
   * Returns a paginated list of ParkingSlots matching the search criteria.
   *
   * @param criteria the {@link ParkingSlotSearchCriteriaTo}.
   * @return the {@link List} of matching {@link ParkingSlotEto}s.
   */
  PaginatedListTo<ParkingSlotEto> findParkingSlotEtos(ParkingSlotSearchCriteriaTo criteria);

  /**
   * Deletes a parkingSlot from the database by its id 'parkingSlotId'.
   *
   * @param parkingSlotId Id of the parkingSlot to delete
   * @return boolean <code>true</code> if the parkingSlot can be deleted, <code>false</code> otherwise
   */
  boolean deleteParkingSlot(Long parkingSlotId);

  /**
   * Saves a parkingSlot and store it in the database.
   *
   * @param parkingSlot the {@link ParkingSlotEto} to create.
   * @return the new {@link ParkingSlotEto} that has been saved with ID and version.
   */
  ParkingSlotEto saveParkingSlot(ParkingSlotEto parkingSlot);

  /**
   * Returns a composite ParkingSlot by its id 'id'.
   *
   * @param id The id 'id' of the ParkingSlot.
   * @return The {@link ParkingSlotCto} with id 'id'
   */
  ParkingSlotCto findParkingSlotCto(Long id);

  /**
   * Returns a paginated list of composite ParkingSlots matching the search criteria.
   *
   * @param criteria the {@link ParkingSlotSearchCriteriaTo}.
   * @return the {@link List} of matching {@link ParkingSlotCto}s.
   */
  PaginatedListTo<ParkingSlotCto> findParkingSlotCtos(ParkingSlotSearchCriteriaTo criteria);

  /**
   * Returns a Reservation by its id 'id'.
   *
   * @param id The id 'id' of the Reservation.
   * @return The {@link ReservationEto} with id 'id'
   */
  ReservationEto findReservation(Long id);

  /**
   * Returns a paginated list of Reservations matching the search criteria.
   *
   * @param criteria the {@link ReservationSearchCriteriaTo}.
   * @return the {@link List} of matching {@link ReservationEto}s.
   */
  PaginatedListTo<ReservationEto> findReservationEtos(ReservationSearchCriteriaTo criteria);

  /**
   * Deletes a reservation from the database by its id 'reservationId'.
   *
   * @param reservationId Id of the reservation to delete
   * @return boolean <code>true</code> if the reservation can be deleted, <code>false</code> otherwise
   */
  boolean deleteReservation(Long reservationId);

  /**
   * Saves a reservation and store it in the database.
   *
   * @param reservation the {@link ReservationEto} to create.
   * @return the new {@link ReservationEto} that has been saved with ID and version.
   */
  ReservationEto saveReservation(ReservationEto reservation);

  /**
   * Returns a composite Reservation by its id 'id'.
   *
   * @param id The id 'id' of the Reservation.
   * @return The {@link ReservationCto} with id 'id'
   */
  ReservationCto findReservationCto(Long id);

  /**
   * Returns a paginated list of composite Reservations matching the search criteria.
   *
   * @param criteria the {@link ReservationSearchCriteriaTo}.
   * @return the {@link List} of matching {@link ReservationCto}s.
   */
  PaginatedListTo<ReservationCto> findReservationCtos(ReservationSearchCriteriaTo criteria);

  /**
   * Returns a ParkingSlotStatus by its id 'id'.
   *
   * @param id The id 'id' of the ParkingSlotStatus.
   * @return The {@link ParkingSlotStatusEto} with id 'id'
   */
  ParkingSlotStatusEto findParkingSlotStatus(Long id);

  /**
   * Returns a paginated list of ParkingSlotStatuss matching the search criteria.
   *
   * @param criteria the {@link ParkingSlotStatusSearchCriteriaTo}.
   * @return the {@link List} of matching {@link ParkingSlotStatusEto}s.
   */
  PaginatedListTo<ParkingSlotStatusEto> findParkingSlotStatusEtos(ParkingSlotStatusSearchCriteriaTo criteria);

  /**
   * Deletes a parkingSlotStatus from the database by its id 'parkingSlotStatusId'.
   *
   * @param parkingSlotStatusId Id of the parkingSlotStatus to delete
   * @return boolean <code>true</code> if the parkingSlotStatus can be deleted, <code>false</code> otherwise
   */
  boolean deleteParkingSlotStatus(Long parkingSlotStatusId);

  /**
   * Saves a parkingSlotStatus and store it in the database.
   *
   * @param parkingSlotStatus the {@link ParkingSlotStatusEto} to create.
   * @return the new {@link ParkingSlotStatusEto} that has been saved with ID and version.
   */
  ParkingSlotStatusEto saveParkingSlotStatus(ParkingSlotStatusEto parkingSlotStatus);

  /**
   * Returns a composite ParkingSlotStatus by its id 'id'.
   *
   * @param id The id 'id' of the ParkingSlotStatus.
   * @return The {@link ParkingSlotStatusCto} with id 'id'
   */
  ParkingSlotStatusCto findParkingSlotStatusCto(Long id);

  /**
   * Returns a paginated list of composite ParkingSlotStatuss matching the search criteria.
   *
   * @param criteria the {@link ParkingSlotStatusSearchCriteriaTo}.
   * @return the {@link List} of matching {@link ParkingSlotStatusCto}s.
   */
  PaginatedListTo<ParkingSlotStatusCto> findParkingSlotStatusCtos(ParkingSlotStatusSearchCriteriaTo criteria);

}
