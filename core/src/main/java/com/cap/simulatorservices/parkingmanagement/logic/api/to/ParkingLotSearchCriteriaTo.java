package com.cap.simulatorservices.parkingmanagement.logic.api.to;

import io.oasp.module.jpa.common.api.to.SearchCriteriaTo;

/**
 * This is the {@link SearchCriteriaTo search criteria} {@link net.sf.mmm.util.transferobject.api.TransferObject TO}
 * used to find {@link com.cap.simulatorservices.parkingmanagement.common.api.ParkingLot}s.
 *
 */
public class ParkingLotSearchCriteriaTo extends SearchCriteriaTo {

  private static final long serialVersionUID = 1L;

  private String address;

  private Double latitude;

  private Double longitude;

  private Long totalNoOfSlots;

  private Long availableNoOfSlots;

  private String operatingcompany;

  /**
   * The constructor.
   */
  public ParkingLotSearchCriteriaTo() {

    super();
  }

  public String getAddress() {

    return this.address;
  }

  public void setAddress(String address) {

    this.address = address;
  }

  public Double getLatitude() {

    return this.latitude;
  }

  public void setLatitude(Double latitude) {

    this.latitude = latitude;
  }

  public Double getLongitude() {

    return this.longitude;
  }

  public void setLongitude(Double longitude) {

    this.longitude = longitude;
  }

  public Long getTotalNoOfSlots() {

    return this.totalNoOfSlots;
  }

  public void setTotalNoOfSlots(Long totalNoOfSlots) {

    this.totalNoOfSlots = totalNoOfSlots;
  }

  public Long getAvailableNoOfSlots() {

    return this.availableNoOfSlots;
  }

  public void setAvailableNoOfSlots(Long availableNoOfSlots) {

    this.availableNoOfSlots = availableNoOfSlots;
  }

  public String getOperatingcompany() {

    return this.operatingcompany;
  }

  public void setOperatingcompany(String operatingcompany) {

    this.operatingcompany = operatingcompany;
  }

}
