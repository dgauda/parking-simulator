package com.cap.simulatorservices.parkingmanagement.logic.api.to;

import com.cap.simulatorservices.general.common.api.to.AbstractCto;

/**
 * Composite transport object of ParkingSlot
 */
public class ParkingSlotCto extends AbstractCto {

  private static final long serialVersionUID = 1L;

  private ParkingSlotEto parkingSlot;

  private ParkingLotEto parkingLotDetails;

  public ParkingSlotEto getParkingSlot() {

    return parkingSlot;
  }

  public void setParkingSlot(ParkingSlotEto parkingSlot) {

    this.parkingSlot = parkingSlot;
  }

  public ParkingLotEto getParkingLotDetails() {

    return parkingLotDetails;
  }

  public void setParkingLotDetails(ParkingLotEto parkingLotDetails) {

    this.parkingLotDetails = parkingLotDetails;
  }

}
