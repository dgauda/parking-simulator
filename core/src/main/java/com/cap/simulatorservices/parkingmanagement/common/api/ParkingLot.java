package com.cap.simulatorservices.parkingmanagement.common.api;

import com.cap.simulatorservices.general.common.api.ApplicationEntity;

public interface ParkingLot extends ApplicationEntity {

  public String getAddress();

  public void setAddress(String address);

  public Double getLatitude();

  public void setLatitude(Double latitude);

  public Double getLongitude();

  public void setLongitude(Double longitude);

  public Long getTotalNoOfSlots();

  public void setTotalNoOfSlots(Long totalNoOfSlots);

  public Long getAvailableNoOfSlots();

  public void setAvailableNoOfSlots(Long availableNoOfSlots);

  public String getOperatingcompany();

  public void setOperatingcompany(String operatingcompany);

}
