
# Make changes accordingly for Minishift or Cluster env.
# Create project to store build base-images
oc new-project devonfw --display-name='DevonFW' --description='DevonFW'

# Create base-images and add them to DevonFW project
oc create -f https://bitbucket.org/dgauda/devonfw-shop-floor/raw/ef77ed723d08da88b9a37b212de4d610632876e8/dsf4openshift/openshift-devonfw-deployment/s2i/java/s2i-devonfw-java-imagestream.json --namespace=devonfw

# Build base-image in DevonFW project
oc start-build s2i-devonfw-java --namespace=devonfw

# Setup the DevonFW project as "image-puller" to be used in other projects in the same cluster
oc policy add-role-to-group system:image-puller system:authenticated --namespace=devonfw

# Use this for MINISHIFT env
# oc login -u system:admin

# Create DevonFW templates into openshift
oc create -f https://bitbucket.org/dgauda/devonfw-shop-floor/raw/ef77ed723d08da88b9a37b212de4d610632876e8/dsf4openshift/openshift-devonfw-deployment/templates/devonfw-java-template.json --namespace=openshift

oc policy add-role-to-user cluster-admin admin
oc policy add-role-to-user admin admin

oc policy add-role-to-user admin system:serviceaccount -n openshift
oc policy add-role-to-user edit system:serviceaccount -n openshift



# Jenkins
oc new-project jenkins --display-name=JENKINS
oc new-app --template=jenkins-ephemeral -p MEMORY_LIMIT=1Gi -l app=jenkins-ephemeral

# for Minishift, use this
# oc new-app --template=jenkins-persistent -p MEMORY_LIMIT=1Gi -l app=jenkins

# Environment Creation
oc new-project dev --display-name=DEV
oc new-project test --display-name=TEST
oc new-project prod --display-name=PROD

# Grant edit access to admin in dev project
oc adm policy add-role-to-user edit admin -n dev

# Grant view access to admin in test project
oc adm policy add-role-to-user view admin -n test

# Grant view access to admin in prod project
oc adm policy add-role-to-user view admin -n prod

# Grant view access to admin in jenkins project
oc adm policy add-role-to-user edit admin -n jenkins

# Grant edit access to jenkins service account
oc policy add-role-to-user edit system:serviceaccount:jenkins:jenkins -n dev
oc policy add-role-to-user edit system:serviceaccount:jenkins:jenkins -n test
oc policy add-role-to-user edit system:serviceaccount:jenkins:jenkins -n prod

# Allow test & prod service account the ability to pull images from dev
oc policy add-role-to-group system:image-puller system:serviceaccounts:test -n dev
oc policy add-role-to-group system:image-puller system:serviceaccounts:prod -n dev

# Create the application definition for your application using oc process from your template created above
# Replace the parameters accordingly
oc process openshift//devonfw-java -p APPLICATION_NAME=parkingapp -p APPLICATION_GROUP_NAME=Parking -p GIT_URI=https://dgauda@bitbucket.org/dgauda/parking-simulator.git -p GIT_REF=master -p CONTEXT_DIR= >parking.json

# Turn automatic image triggers off and set the initial number of replicas to 0

sed '1,/\"automatic\": true/s/\"automatic\": true/\"automatic\": false/' parking.json >parking-nobuild.json

sed '1,/\"replicas\": 1/s/\"replicas\": 1/\"replicas\": 0/' parking-nobuild.json >parking-zero.json
   
oc project dev

# create the application using newly created json file
oc create -f parking-zero.json

# Create QA and PROD environments by pointing to the tags for not-yet-created image stream

oc project test

oc new-app dev/parkingapp:qa --name parkingapp-qa --allow-missing-imagestream-tags=true

# oc new-app command does not create a service or route, addding these manually

oc set triggers dc/parkingapp-qa --manual
oc expose dc/parkingapp-qa --port=8080
oc expose svc parkingapp-qa

oc project prod

# Create the blue and green applications
oc new-app dev/parkingapp:prod --name="app-green" --allow-missing-imagestream-tags=true
oc new-app dev/parkingapp:prod --name="app-blue" --allow-missing-imagestream-tags=true

# Removes the triggers
oc set triggers dc/app-green --remove-all
oc set triggers dc/app-blue --remove-all

oc expose dc/app-blue --port 8080
oc expose dc/app-green --port 8080

oc expose svc/app-green --name blue-green

#
# Pipelines deployment
#
oc project jenkins

# Creates the pipelines
oc new-app https://dgauda@bitbucket.org/dgauda/parking-simulator#master --context-dir=cicd/pipelines/ci --name ci-pipeline
oc new-app https://dgauda@bitbucket.org/dgauda/parking-simulator#master --context-dir=cicd/pipelines/cd --name cd-pipeline
oc new-app https://dgauda@bitbucket.org/dgauda/parking-simulator#master --context-dir=cicd/pipelines/bg --name bg-pipeline
oc new-app https://dgauda@bitbucket.org/dgauda/parking-simulator#master --context-dir=cicd/pipelines/ab --name ab-pipeline

